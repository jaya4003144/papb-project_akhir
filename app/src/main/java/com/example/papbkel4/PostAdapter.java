package com.example.papbkel4;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.ViewHolder> {

    private List<PostModel> postList;
    private Context context;
    private OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public PostAdapter(Context context, List<PostModel> postList) {
        this.context = context;
        this.postList = postList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_questionhome, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PostModel post = postList.get(position);

        holder.usernameTextView.setText(post.getUsername());
        holder.questionTextView.setText(post.getQuestion());
        holder.dateTextView.setText(post.getDate());

        // Mengatur OnClickListener untuk TextView comments1
        holder.comments1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Mengarahkan ke AnswerPage saat TextView comments1 diklik
                Intent answerIntent = new Intent(context, AnswerPage.class);
                answerIntent.putExtra("POST_DETAIL", post);
                context.startActivity(answerIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return postList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView usernameTextView;
        TextView questionTextView;
        TextView dateTextView;
        TextView comments1;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            usernameTextView = itemView.findViewById(R.id.namapengguna);
            questionTextView = itemView.findViewById(R.id.textView1);
            dateTextView = itemView.findViewById(R.id.tanggal);
            comments1 = itemView.findViewById(R.id.comments1); // Mengambil TextView comments1 dari layout item_questionhome
        }
    }
}