
package com.example.papbkel4;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class ProfilePicturePage extends AppCompatActivity {
    private StorageReference storageReference;
    private static final int PICK_IMAGE_REQUEST = 1;
    private ImageView profPicImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_picture_page);

        storageReference = FirebaseStorage.getInstance().getReference();
        profPicImageView = findViewById(R.id.profpic); // Ganti dengan ID ImageView yang sesuai

        // Handling the image selection
        findViewById(R.id.ic_upload).setOnClickListener(v -> openFileChooser());
        findViewById(R.id.ic_download).setOnClickListener(v -> downloadProfilePicture());
    }

    private void openFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK) {
            if (data != null && data.getData() != null) {
                Uri imageUri = data.getData();
                uploadProfilePicture(imageUri);
            }
        }
    }

    private void uploadProfilePicture(Uri imageUri) {
        StorageReference ref = storageReference.child("images/profile.jpg");

        ref.putFile(imageUri)
                .addOnSuccessListener(taskSnapshot -> {
                    ref.getDownloadUrl().addOnSuccessListener(uri -> {
                        String downloadUrl = uri.toString();

                        Intent resultIntent = new Intent();
                        resultIntent.putExtra("IMAGE_URL", downloadUrl);
                        setResult(RESULT_OK, resultIntent);
                        finish();
                    });
                })
                .addOnFailureListener(e -> {
                    Toast.makeText(ProfilePicturePage.this, "Failed to upload image", Toast.LENGTH_SHORT).show();
                });
    }

    private void downloadProfilePicture() {
        // Menentukan referensi file di Firebase Storage
        StorageReference ref = storageReference.child("images/profile.jpg");

        // Mengunduh gambar dari Firebase Storage ke ImageView
        ref.getDownloadUrl()
                .addOnSuccessListener(uri -> {
                    // Gambar berhasil diunduh
                    // Menggunakan Glide untuk menampilkan gambar di ImageView
                    Glide.with(this)
                            .load(uri)
                            .into(profPicImageView);
                    Toast.makeText(ProfilePicturePage.this, "Gambar berhasil diunduh", Toast.LENGTH_SHORT).show();
                })
                .addOnFailureListener(e -> {
                    // Error saat mengunduh gambar
                    Log.e("ProfilePicturePage", "Gagal mengunduh gambar: " + e.getMessage()); // Menambahkan log pesan kesalahan
                    Toast.makeText(ProfilePicturePage.this, "Gagal mengunduh gambar", Toast.LENGTH_SHORT).show();
                });
    }
}
