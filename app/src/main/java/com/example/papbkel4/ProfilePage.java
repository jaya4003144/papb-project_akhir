package com.example.papbkel4;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ProfilePage extends AppCompatActivity {
    private ImageView profile_pic;
    Button btLogOut;
    FirebaseAuth auth;
    TextView email, username;

    private static final int PICK_IMAGE_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_page);

        profile_pic = findViewById(R.id.profile_pic);
        auth = FirebaseAuth.getInstance();
        btLogOut = findViewById(R.id.btLogOut);
        email = findViewById(R.id.email);
        username = findViewById(R.id.username);

        FirebaseUser user = auth.getCurrentUser();

        if (user != null) {
            email.setText(user.getEmail());
        }

        // Set username pada TextView username (5 huruf pertama dari email)
        if (user != null && user.getEmail() != null && user.getEmail().length() >= 5) {
            String firstFiveChars = user.getEmail().substring(0, 5);
            username.setText(firstFiveChars);
        }

        btLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                auth.signOut();
                startActivity(new Intent(ProfilePage.this, Login.class));
                finish();
         }
});

        // Cek apakah ada URL gambar yang tersimpan di database saat aplikasi dimuat kembali
        loadImageUrlFromFirebaseDatabase();

        profile_pic.setOnClickListener(v -> {
            Intent intent = new Intent(ProfilePage.this, ProfilePicturePage.class);
            startActivityForResult(intent, PICK_IMAGE_REQUEST);
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK) {
            if (data != null) {
                String imageUrl = data.getStringExtra("IMAGE_URL");
                Glide.with(this)
                        .load(imageUrl)
                        .circleCrop()
                        .into(profile_pic);

                // Simpan URL gambar yang dipilih pengguna ke Firebase Realtime Database
                saveImageUrlToFirebaseDatabase(imageUrl);
            }
        }
    }

    // Metode untuk menyimpan URL gambar ke Firebase Realtime Database
    private void saveImageUrlToFirebaseDatabase(String imageUrl) {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            String userId = currentUser.getUid();
            // Mengatur path penyimpanan URL gambar profil pengguna
            DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("users")
                    .child(userId)
                    .child("profilePicUrl");
            databaseRef.setValue(imageUrl);
        }
    }

    // Metode untuk mendapatkan URL gambar dari Firebase Realtime Database
    private void loadImageUrlFromFirebaseDatabase() {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            String userId = currentUser.getUid();
            // Mendapatkan URL gambar profil pengguna dari Firebase Realtime Database
            DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("users")
                    .child(userId)
                    .child("profilePicUrl");
            databaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        String imageUrl = dataSnapshot.getValue(String.class);
                        if (imageUrl != null) {
                            // Mengatur gambar profil pengguna dari URL yang didapatkan
                            Glide.with(ProfilePage.this)
                                    .load(imageUrl)
                                    .circleCrop()
                                    .into(profile_pic);
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    // Handle error
                }
            });

        }

    }
}
