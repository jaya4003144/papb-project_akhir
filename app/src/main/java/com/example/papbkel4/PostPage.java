package com.example.papbkel4;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import androidx.appcompat.app.AppCompatActivity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.util.Locale;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class PostPage extends AppCompatActivity implements Serializable {
    ImageButton bthome, btsearch, btpost, btnotif, btprofile;
    private EditText etQuestion;
    private Button btnPost;
    private DatabaseReference databaseReference;
    private String username;
    private String question;
    private String date;

    public PostPage() {
    }

    public PostPage(String username, String question, String date) {
        this.username = username;
        this.question = question;
        this.date = date;
    }

    public String getUsername() {
        return username;
    }

    public String getQuestion() {
        return question;
    }

    public String getDate() {
        return date;
    }


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.post_page);

        databaseReference = FirebaseDatabase.getInstance().getReference("posts");
        //

        etQuestion = findViewById(R.id.etquestion);
        btnPost = findViewById(R.id.btnpost);

        btnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String postedText = etQuestion.getText().toString();

                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                // Mendapatkan email pengguna

                String username = null;
                if (user != null) {
                    username = user.getEmail().substring(0, Math.min(user.getEmail().length(), 5)); // Gunakan email sebagai username
                }

                String currentDate = getCurrentDateTime();
                PostModel newPost = new PostModel(username, postedText, currentDate);

                String postId = databaseReference.push().getKey();  // Generate a unique key
                databaseReference.child(postId).setValue(newPost);


                List<PostModel> postList = new ArrayList<>();

                // Assuming you have a list of posts in your homepage
                if (getIntent().hasExtra("POST_LIST")) {
                    postList = (List<PostModel>) getIntent().getSerializableExtra("POST_LIST");
                }

                // Add the new post to the list
                postList.add(newPost);

                // Pass the updated list to the homepage
                Intent homeIntent = new Intent(getApplicationContext(), HomePage.class);
//                homeIntent.putExtra("POST_LIST", (ArrayList<Post>) postList);
                startActivity(homeIntent);
            }
        });

        //        footer
        bthome = findViewById(R.id.bthome);
        btsearch = findViewById(R.id.btsearch);
        btpost = findViewById(R.id.btpost);
        btnotif = findViewById(R.id.btnotif);
        btprofile = findViewById(R.id.btprofile);

        bthome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent bthome = new Intent(getApplicationContext(), HomePage.class);
                startActivity(bthome);
            }
        });
        btsearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent btsearch = new Intent(getApplicationContext(), SearchPage.class);
                startActivity(btsearch);
            }
        });
        btpost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent btpost = new Intent(getApplicationContext(), PostPage.class);
                startActivity(btpost);
            }
        });
        btnotif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent btnotif = new Intent(getApplicationContext(), NotificationPage.class);
                startActivity(btnotif);
            }
        });
        btprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent btprofile = new Intent(getApplicationContext(), ProfilePage.class);
                startActivity(btprofile);
            }
        });
    }
    private String getCurrentDateTime() {
        // Get the current date and time
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        return dateFormat.format(calendar.getTime());
    }
}
