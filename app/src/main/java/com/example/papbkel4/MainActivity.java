package com.example.papbkel4;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    Handler handler = new Handler();
    FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_page);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();



        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (user!=null) {
                    startActivity(new Intent(MainActivity.this, HomePage.class));
                    finish();
                } else {
                    Intent intent = new Intent(MainActivity.this, Login.class);
                    startActivity(intent);
                    finish();
                }

            }
        }, 3000);
    }



}