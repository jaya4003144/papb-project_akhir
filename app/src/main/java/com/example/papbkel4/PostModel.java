package com.example.papbkel4;

import java.io.Serializable;

public class PostModel implements Serializable {
    private String username;
    private String question;
    private String date;


    public PostModel() {

    }


    public PostModel(String username, String question, String date) {
        this.username = username;
        this.question = question;
        this.date = date;
    }



    public String getUsername() {
        return username;
    }

    public String getQuestion() {
        return question;
    }

    public String getDate() {
        return date;
    }



}