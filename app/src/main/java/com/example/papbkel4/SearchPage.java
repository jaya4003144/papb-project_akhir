package com.example.papbkel4;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;

public class SearchPage extends AppCompatActivity {
    ImageButton bthome, btsearch, btpost, btnotif,btprofile;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_page);

        //        footer
        bthome = findViewById(R.id.bthome);
        btsearch = findViewById(R.id.btsearch);
        btpost = findViewById(R.id.btpost);
        btnotif = findViewById(R.id.btnotif);
        btprofile = findViewById(R.id.btprofile);

        bthome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent bthome = new Intent(getApplicationContext(), HomePage.class);
                startActivity(bthome);
            }
        });
        btsearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent btsearch = new Intent(getApplicationContext(), SearchPage.class);
                startActivity(btsearch);
            }
        });
        btpost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent btpost = new Intent(getApplicationContext(), PostPage.class);
                startActivity(btpost);
            }
        });
        btnotif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent btnotif = new Intent(getApplicationContext(), NotificationPage.class);
                startActivity(btnotif);
            }
        });
        btprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent btprofile = new Intent(getApplicationContext(), ProfilePage.class);
                startActivity(btprofile);
            }
        });
    }
}

