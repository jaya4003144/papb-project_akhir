package com.example.papbkel4;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class RegisterPage extends AppCompatActivity {
    private TextView tvToLogin;
    private Button btRegister;
    private EditText etUsername, etEmail, etPassword, etConfPassword;
    private FirebaseAuth auth;

    protected void onCreate(Bundle savedInstanceState) {
        FirebaseApp.initializeApp(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_page);

        etUsername = findViewById(R.id.etUsername);
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        etConfPassword = findViewById(R.id.etConfPassword);
        tvToLogin = findViewById(R.id.tvToLogin);
        btRegister = findViewById(R.id.btRegister);

        auth = FirebaseAuth.getInstance();

        tvToLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), Login.class));
            }
        });

        btRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = etUsername.getText().toString();
                String email = etEmail.getText().toString();
                String password = etPassword.getText().toString();
                String confirm = etConfPassword.getText().toString();

                if (!(username.isEmpty() || email.isEmpty() || password.isEmpty() || confirm.isEmpty())) {
                    if (confirm.equals(password)){
                        auth.createUserWithEmailAndPassword(email, password)
                                .addOnCompleteListener(RegisterPage.this, new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (task.isSuccessful()){
                                            auth.getCurrentUser().sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (task.isSuccessful()){
                                                        Toast.makeText(getApplicationContext(), "Daftar Berhasil. Silahkan Cek Email Anda!!", Toast.LENGTH_SHORT).show();

                                                        startActivity(new Intent(getApplicationContext(), Login.class));
                                                    }else{
                                                        Toast.makeText(getApplicationContext(), "Verifikasi Gagal Di Kirim", Toast.LENGTH_SHORT).show();
                                                    }
                                                }
                                            });
                                        }else{
                                            Toast.makeText(getApplicationContext(), "Daftar Gagal", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                    } else {
                        etConfPassword.setError("Password Tidak Sesuai");
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Terdapat Data yang Masih Kosong", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
