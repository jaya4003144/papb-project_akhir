package com.example.papbkel4;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.papbkel4.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class HomePage extends AppCompatActivity {
    ImageButton bthome, btsearch, btpost, btnotif,btprofile;

    RecyclerView recyclerView;
    PostAdapter postAdapter;
    private DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {

            String username = user.getEmail(); // Mendapatkan email pengguna

        } else {

            startActivity(new Intent(getApplicationContext(), Login.class));
            finish(); // Menutup halaman homepage agar tidak dapat kembali saat menekan tombol back
            return;
        }



        setContentView(R.layout.home_page);
        String username = user.getEmail();
//        greetingText = findViewById(R.id.greet);
        recyclerView = findViewById(R.id.question);

        // Initialize the database reference
        databaseReference = FirebaseDatabase.getInstance().getReference("posts");

        // Retrieve data from Firebase
        retrieveDataFromFirebase();

        // Get the posted text from the intent
        Intent intent = getIntent();
        if (intent.hasExtra("POST_LIST")) {
            List<PostModel> postList = (List<PostModel>) intent.getSerializableExtra("POST_LIST");

            postAdapter = new PostAdapter(this, postList);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(postAdapter);

            // Set click listener untuk menangani pemanggilan AnswerPage saat item di RecyclerView diklik
            postAdapter.setOnItemClickListener(new PostAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(int position) {
                    // Handle the item click here
                    // Ambil data dari item yang dipilih (dalam hal ini, misalnya PostModel)
                    PostModel selectedPost = postList.get(position);

                    // Lakukan intent ke AnswerPage, sertakan data yang dibutuhkan jika ada
                    Intent answerIntent = new Intent(HomePage.this, AnswerPage.class);
                    // Sertakan data dari item yang dipilih ke AnswerPage
                    answerIntent.putExtra("SELECTED_POST", selectedPost);
                    startActivity(answerIntent);
                }
            });
        }


        //        footer
        bthome = findViewById(R.id.bthome);
        btsearch = findViewById(R.id.btsearch);
        btpost = findViewById(R.id.btpost);
        btnotif = findViewById(R.id.btnotif);
        btprofile = findViewById(R.id.btprofile);

        bthome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent bthome = new Intent(getApplicationContext(), HomePage.class);
                startActivity(bthome);
            }
        });
        btsearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent btsearch = new Intent(getApplicationContext(), SearchPage.class);
                startActivity(btsearch);
            }
        });
        btpost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent btpost = new Intent(getApplicationContext(), PostPage.class);
                startActivity(btpost);
            }
        });
        btnotif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent btnotif = new Intent(getApplicationContext(), NotificationPage.class);
                startActivity(btnotif);
            }
        });
        btprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent btprofile = new Intent(getApplicationContext(), ProfilePage.class);
                startActivity(btprofile);
            }
        });
    }

    private void retrieveDataFromFirebase() {
        // Menambahkan ValueEventListener ke databaseReference untuk mendapatkan data dari Firebase Database
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                List<PostModel> postList = new ArrayList<>(); // Membuat list untuk menyimpan data post

                // Iterasi melalui setiap child (anak) dalam node "posts"
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    // Mendeserialize objek Post dari DataSnapshot
                    PostModel post = postSnapshot.getValue(PostModel.class); // Mengambil data post dari Firebase dan mengonversinya ke objek PostModel
                    postList.add(post); // Menambahkan data post ke dalam list
                }

                // Memperbarui adapter RecyclerView dengan data baru yang telah diambil dari Firebase
                postAdapter = new PostAdapter(HomePage.this, postList); // Membuat adapter baru dengan data yang telah diambil
                recyclerView.setLayoutManager(new LinearLayoutManager(HomePage.this)); // Mengatur layout manager untuk RecyclerView
                recyclerView.setAdapter(postAdapter); // Mengatur adapter RecyclerView dengan data yang baru
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                // Handle the error
            }
        });
    }
}